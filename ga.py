import subprocess
from random import randint


def GenerateRandom(n):
    n1 = 0
    array = []
    while n != n1:
        array.append(randint(1, 5))
        n1 = n1 + 1
    return array


def fit(individuo):
    n = 0
    uns = 0
    steps = ''
    args = ['./game']
    while len(individuo) != n:
        if individuo[n] == 1:
            args.append('w')
        if individuo[n] == 2:
            args.append('s')
        if individuo[n] == 3:
            args.append('a')
        if individuo[n] == 4:
            args.append('d')
        if individuo[n] == 5:
            args.append(' ')
        n = n + 1
    print str(args)
    proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
    #proc.communicate(input=text.encode('utf-8'))
    line = ''
    while proc.poll() is None:
        line = proc.stdout.readline()
    #print line
    a = int(line.count('1'))
    a += int(line.count('2')) *2
    a += int(line.count('3')) *3
    print 'passos concluidos:\t'+ str(line.count('1'))
    print 'Enimigos mortos:\t'+ str(line.count('2'))
    print 'Enimigos grandes mortos:\t'+ str(line.count('3'))
    print 'Score:\t'+ str(a)
    return a


def Max(populacao, qt):
    array = [None] * (len(populacao[0]) + 1)
    n = 0
    while n != len(populacao):
        i = fit(populacao[n])
        while (array[i] is not None )&( i != 0):
            i = i - 1
        array[i] = n
        n = n + 1
    array_final = []
    j = len(array) - 1
    n = 0
    while n != qt:
        while array[j] is None:
            j = j - 1
        array_final.append(array[j])
        n = n + 1
        j = j - 1

    return array_final


def mutar(individuo):
    i = randint(0, len(individuo) - 1)
    if individuo[i] == 0:
        individuo[i] = 1
    else:
        individuo[i] = 0
    return individuo

def CrossOver(Pai,Mae):
    n = len(Mae)-1
    corte = randint(0,n)
    Filho = Pai[0:corte]+Mae[corte:n+1]
    Filha = Mae[0:corte] + Pai[corte:n + 1]
    return [Filho,Filha]


Num_pop = 40
Genes = 400
mutacoes = 5
enemy = 30;
max_enemy = 2
populacao = []
n = 0
while (n != Num_pop):
    populacao.append(GenerateRandom(Genes))
    n = n + 1
i = 0
while i != 100000:
    pp = 0
    while pp != len(populacao):
        if fit(populacao[pp]) >= Genes + (enemy*2) +(max_enemy *3):
            print(
                '------------------------------------------------------------------------------------------------------------')
            print(
                '------------------------------------------------------------------------------------------------------------')
            print(
                '------------------------------------------------------------------------------------------------------------')
            print('Posicao:')
            print(pp)
            print('Individuo:')
            print(populacao[pp])
            print('Score:')
            print(fit(populacao[pp]))
            print('Interacao:')
            print(i)
            print(
                '------------------------------------------------------------------------------------------------------------')
            print(
                '------------------------------------------------------------------------------------------------------------')
            print(
                '------------------------------------------------------------------------------------------------------------')
            exit()
        pp = pp + 1
    otimos = Max(populacao, Num_pop / 2)
    print(otimos)
    n = 0
    populacao_backup = populacao
    populacao = []
    while n != len(otimos):
        populacao.append(populacao_backup[otimos[n]])
        n = n + 1
    kk = 0
    while mutacoes > kk:
        k = randint(0, len(populacao) - 1)
        populacao[k] = mutar(populacao[k])
        kk = kk + 1
    f1 = 0
    f2 = 1
    pop =  len(populacao)
    while f2 < pop:
        array_filho = CrossOver(populacao[f1],populacao[f2])
        populacao.append(array_filho[0])
        populacao.append(array_filho[1])
        #print(array_filho[0])
        #f1 = f1 + 2
        f2 = f2 + 2
    i = i + 1