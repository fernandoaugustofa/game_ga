#include <GL/glut.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>  
#include <string.h>  
#include "SOIL.h"


 
int screen_width=800;
int screen_height=800;
float x = 0;
float y = -160;
float old_x = 0;
float old_y = 0;
int shot = 0;
int shot_inativo = 0;
float old_shot = 0;
GLuint texture[1];
float vetor_enemy[100][4] = {0};
float vetor_shot[10000][2] = {0};
float vetor_explosion[10000][3] = {0};
char nave[18];
void display();
int multi = 1;
int explosion = 0;
int i = 0;
bool KeyDown[256];
int life = 100;
int n_e = 30;
int n_max_e = 2;
int anim = 0;
bool pass = true;
bool ga = true;
int steps[1000]= {0};
int step = 0;
int quadro = 0;


bool LoadGLTextures(char* img)
{
	int width, height;
	texture[0] = SOIL_load_OGL_texture 
	(
		img,
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT| SOIL_FLAG_MULTIPLY_ALPHA 
	);
	if(texture[0] == 0)
		return false;
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	return true;
}

void DefineIluminacao (void)
{
	GLfloat light_pos1[] = {0.0f, 0.0f, -15.0f, 1.0f};
	GLfloat light_ambient[] = {100.0f, 100.0f, 100.0f, 100.0f};
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos1);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);  
}
int inicia_enemy(int id, bool max = false){
	vetor_enemy[id][0] = (rand() % 320) -160;
	vetor_enemy[id][1] = (rand() % 320) -160;
    vetor_enemy[id][2] = 1;
    int randomNum = rand() % 160 + (-160);
    vetor_enemy[id][3] = -200;
    if(max){
        int randomNum = rand() % 50 + (-25);
        vetor_enemy[id][1] = (rand() % 200) -600;
        vetor_enemy[id][2] = randomNum;
    }
}

void shot_view(int shot_x, int shot_y){
    glPushMatrix();
        glTranslatef(shot_y,shot_x,0);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        LoadGLTextures("bullet.png");
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);  glVertex3f(0,0,0);
            glTexCoord2f(1,0);  glVertex3f(8,0,0);
            glTexCoord2f(1,1);  glVertex3f(8,16,0);
            glTexCoord2f(0,1);  glVertex3f(0,16,0);
        glEnd();
    glPopMatrix();
}

void explosion_view(int shot_x, int shot_y){
    glPushMatrix();
        glTranslatef(-15 +shot_y, -15 +shot_x,0);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        LoadGLTextures("explosion.png");
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);  glVertex3f(0,0,0);
            glTexCoord2f(1,0);  glVertex3f(30,0,0);
            glTexCoord2f(1,1);  glVertex3f(30,30,0);
            glTexCoord2f(0,1);  glVertex3f(0,30,0);
        glEnd();
    glPopMatrix();
}

bool CheckCollision(int x, int y, int x2, int y2, int size_i, int size_j)
{
    bool collisionX = y + 8 >= x2 && x2 + size_i >= y;
    bool collisionY = x + 16 >= y2 && y2 + size_j >= x;
    return collisionX && collisionY;
}  

bool CheckCollisionplayer(int x, int y, int x2, int y2, int size_i, int size_j)
{
    bool collisionX = y + 26 >= x2 && x2 + size_i >= y;
    bool collisionY = x + 20 >= y2 && y2 + size_j >= x;
    return collisionX && collisionY;
}  

void enemy(int id){
    if(vetor_enemy[id][2] == 1 && vetor_enemy[id][1] <= -160){
        vetor_enemy[id][2] = -1;
    }
    if(vetor_enemy[id][2] == -1 && vetor_enemy[id][1] >= 160){
        vetor_enemy[id][2] = 1;
    }
    if (vetor_enemy[id][2] == 1){
        LoadGLTextures("enemy1.png");
    }else{
        LoadGLTextures("enemy-1.png");
    }
    vetor_enemy[id][1] -= 1 * vetor_enemy[id][2];
    for(int i = shot_inativo; i < shot; i++){
	   if(CheckCollision(vetor_shot[i][0], vetor_shot[i][1],vetor_enemy[id][0],vetor_enemy[id][1],26,20)){
        printf("%s", "2");
        vetor_explosion[explosion][0] = vetor_enemy[id][0];
        vetor_explosion[explosion][1] = vetor_enemy[id][1];
        vetor_explosion[explosion][2] = 200;
        vetor_enemy[id][0] = -2000;
        vetor_enemy[id][1] = -2000;
        vetor_shot[i][0] = -1000;
        vetor_shot[i][1] = -1000;
        }
    }
	glPushMatrix();
 		glTranslatef(vetor_enemy[id][0],vetor_enemy[id][1],0.0);
		glBindTexture(GL_TEXTURE_2D, texture[0]);
		glBegin(GL_QUADS);
            glTexCoord2f(0,0);  glVertex3f(0,0,0);
            glTexCoord2f(1,0);  glVertex3f(26,0,0);
            glTexCoord2f(1,1);  glVertex3f(26,20,0);
            glTexCoord2f(0,1);  glVertex3f(0,20,0);
        glEnd();
 	glPopMatrix();
}

void max_enemy(int id){
     LoadGLTextures("max_plane.png");
    if (vetor_enemy[id][1] == 20 + vetor_enemy[id][2]){
        
    }else{
        vetor_enemy[id][1] += 0.4;
    }
    if(vetor_enemy[id][3] > 10){
        LoadGLTextures("max_plane_d.png");
    }
    for(int i = shot_inativo; i < shot; i++){
       if(CheckCollision(vetor_shot[i][0], vetor_shot[i][1],vetor_enemy[id][0],vetor_enemy[id][1],90,80)){
        printf("%s", "3");
        vetor_enemy[id][0] = -2000;
        vetor_enemy[id][1] = -2000;
        vetor_shot[i][0] = -1000;
        vetor_shot[i][1] = -1000;
        }
    }
    glPushMatrix();
        glTranslatef(vetor_enemy[id][0],vetor_enemy[id][1],0.0);
        glBindTexture(GL_TEXTURE_2D, texture[0]);
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);  glVertex3f(0,0,0);
            glTexCoord2f(1,0);  glVertex3f(90,0,0);
            glTexCoord2f(1,1);  glVertex3f(90,80,0);
            glTexCoord2f(0,1);  glVertex3f(0,80,0);
        glEnd();
    glPopMatrix();
}

void move()
{
    if(KeyDown['w']){
        if(y < 160){
            y = y + 3;
        }
    }
    if(KeyDown['s']){
        if(y > -160){
            y = y - 3;
        }
    }
    if(KeyDown['a']){
        if(x < 160){
            strcpy(nave,"nave_d_m.png");
            x=x+3;
        }
    }
    if(KeyDown['d']){
        if(x > -160){
            strcpy(nave,"nave_e_m.png");
            x=x-3;
        }
    }
    if(KeyDown[' ']){
            shot+=1;
            KeyDown[' '] = false;
    }
    if(KeyDown['a'] == false && KeyDown['d'] == false){
        strcpy(nave,"nave.png");
    }
}

void Draw (){
	if(pass){
        if(ga){
            if(quadro != 10){
                quadro++;
            }else{
                quadro = 0;
                KeyDown[steps[step]] = true;
                step++;
                if (steps[step] == 0){
                    exit(0);
                }
            }
        }
        move();

    }
    glPushMatrix();
    LoadGLTextures("logo.png");
    glTranslatef( x, -160, 0 );
    glBegin(GL_QUADS);
        glTexCoord2i(0,0); glVertex2i(0, 0);
        glTexCoord2i(1,0); glVertex2i(life, 0);
        glTexCoord2i(1,1); glVertex2i(life, 10);
        glTexCoord2i(0,1); glVertex2i(0, 10);
    glEnd();
    glPopMatrix();
    glEnable(GL_BLEND);
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	LoadGLTextures(nave);
    glPushMatrix();
    LoadGLTextures("water.png");
    glTranslatef( -screen_width/2, -screen_height/2, 0 );
    glBegin(GL_QUADS);
        glTexCoord2i(0,0); glVertex2i(0, 0);
        glTexCoord2i(1,0); glVertex2i(screen_width, 0);
        glTexCoord2i(1,1); glVertex2i(screen_width, screen_height);
        glTexCoord2i(0,1); glVertex2i(0, screen_height);
    glEnd();
    glPopMatrix();
	DefineIluminacao();
    i = (rand() %160) -1;
    for(int plane = 0; plane != n_e;plane++){
    	if(pass){
    		if(CheckCollision(y, x,vetor_enemy[plane][0],vetor_enemy[plane][1],26,20)){
    			printf("%s", "0");
                if(ga){
                    exit(0);
                }
    			pass = false;
    		}
    	}
		enemy(plane);
   	}
   	if(pass){
   		for(int y = 0; y != 256;y++){
   			if(KeyDown[y]){
   				printf("%s", "1");
                if(ga){
                    KeyDown[y] = false;
                }
   			}
   		}
   	
   	for(int plane = n_e; plane != (n_max_e + n_e);plane++){
		max_enemy(plane);
	}
	glPushMatrix();
		glTranslatef(x,y,0);
		LoadGLTextures(nave);
		glBegin(GL_QUADS);
			glTexCoord2f(0,0);	glVertex3f(0,0,0);
    		glTexCoord2f(1,0);	glVertex3f(26,0,0);
    		glTexCoord2f(1,1);	glVertex3f(26,20,0);
    		glTexCoord2f(0,1);	glVertex3f(0,20,0);
		glEnd();
 	glPopMatrix();
 	if(old_shot != shot){
		vetor_shot[shot][0] = y;
		vetor_shot[shot][1] = x;
 	}
 	old_shot = shot;
	for(int i = shot_inativo; i < shot; i++){
		vetor_shot[i][0] += 3.5;
		shot_view(vetor_shot[i][0], vetor_shot[i][1]);
        if(vetor_shot[i][0] >= 160){
            shot_inativo++;
        }
	}
}
}

void display() {
	glEnable(GL_TEXTURE_2D);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);  
    glViewport(0, 0, (GLsizei)screen_width, (GLsizei)screen_width);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(50, (GLfloat)screen_width/(GLfloat)screen_height, 10.0, -15.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt( 0.0, 0.0f, -450, 0.0, 0.0f,0.0, 0.0f, 1.0f, 0.0f);
    Draw();
    glutSwapBuffers();
}

void reshape (int width, int height) {
    
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // We clear both the color and the depth buffer so to draw the next frame
    glViewport(0, 0, (GLsizei)width, (GLsizei)height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(50, (GLfloat)width/(GLfloat)height, 10, -15.0);

    glutPostRedisplay();
    
}

void keyDown(unsigned char Key, int _x, int _y)
{
	KeyDown[Key]=true;
	return;
}



void keyUp(unsigned char key, int _x, int _y)
{
    KeyDown[key]=false;
    return;
}



int main (int argc, char **argv) {  
	srand (56);
	for(int plane = 0; plane != n_e;plane++){
		inicia_enemy(plane);
	}
	for(int plane = n_e; plane != (n_max_e + n_e);plane++){
		inicia_enemy(plane, true);
	}
    for (i=1; i < argc; i++) {
        steps[i-1] = (int)argv[i][0];
    }
    glutInit(&argc, argv);
    glutInitDisplayMode (GLUT_DOUBLE); 
    glutInitWindowSize (screen_width, screen_width); 
    glutInitWindowPosition (400, 100);
    glutCreateWindow ("game");
    glutDisplayFunc(display);
    glutIdleFunc (display); 
    glutReshapeFunc(reshape);
    glutKeyboardUpFunc(keyUp);
    glutKeyboardFunc(keyDown);
    glutMainLoop();
}  